<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(DEVICE)</name>
  <scripts>
    <script file="EmbeddedPy">
      <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

macros = widget.getPropertyValue("macros")

def ch_x(idx):
    devname = PVUtil.getString(pvs[idx - 1])
    macros.add("DEVICE_CH" + str(idx), devname)
    ch_widget = ScriptUtil.findWidgetByName(widget, "Open Ch" + str(idx))
    if devname:
        ch_widget.setPropertyValue("text", devname)
    else:
        ch_widget.setPropertyValue("enabled", False)

ch_x(1)
ch_x(2)
widget.setPropertyValue("macros", macros)
]]></text>
      <pv_name>$(DEVICE):Ch1DevNameR</pv_name>
      <pv_name>$(DEVICE):Ch2DevNameR</pv_name>
    </script>
  </scripts>
  <widget type="label" version="2.0.0">
    <name>LM510</name>
    <text>Cryogenics Level Monitor $(DEVICE)</text>
    <width>800</width>
    <height>25</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Manufacturer</name>
    <text>Manufacturer:</text>
    <x>10</x>
    <y>35</y>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Model</name>
    <text>Model:</text>
    <x>10</x>
    <y>65</y>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Serial</name>
    <text>Serial:</text>
    <x>10</x>
    <y>95</y>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Firmware</name>
    <text>Firmware:</text>
    <x>10</x>
    <y>125</y>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>ManufR</name>
    <pv_name>$(DEVICE):ManufR</pv_name>
    <x>120</x>
    <y>35</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>ModelR</name>
    <pv_name>$(DEVICE):ModelR</pv_name>
    <x>120</x>
    <y>65</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>SerNumR</name>
    <pv_name>$(DEVICE):SerNumR</pv_name>
    <x>120</x>
    <y>95</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>FWVerR</name>
    <pv_name>$(DEVICE):FWVerR</pv_name>
    <x>120</x>
    <y>125</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="navtabs" version="2.0.0">
    <name>Navigation Tabs</name>
    <tabs>
      <tab>
        <name>Channel 1</name>
        <file>lm510_lt.bob</file>
        <macros>
          <DEVICE>$(DEVICE_CH1)</DEVICE>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>Channel 2</name>
        <file>lm510_lt.bob</file>
        <macros>
          <DEVICE>$(DEVICE_CH2)</DEVICE>
        </macros>
        <group_name></group_name>
      </tab>
    </tabs>
    <x>10</x>
    <y>170</y>
    <width>780</width>
    <height>430</height>
    <direction>0</direction>
    <selected_color>
      <color name="GREEN-BORDER" red="40" green="140" blue="40">
      </color>
    </selected_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Operation Mode</name>
    <text>Operation Mode:</text>
    <x>270</x>
    <y>35</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Operation Mode</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Mode-RB</name>
    <pv_name>$(DEVICE):Mode-RB</pv_name>
    <x>400</x>
    <y>35</y>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>CommsStatR</name>
    <pv_name>$(DEVICE):CommsStatR</pv_name>
    <x>627</x>
    <y>3</y>
    <width>170</width>
    <transparent>true</transparent>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Control mode</name>
    <text>Control mode:</text>
    <x>270</x>
    <y>65</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="combo" version="2.0.0">
    <name>CtrlModeS</name>
    <pv_name>$(DEVICE):CtrlModeS</pv_name>
    <x>400</x>
    <y>65</y>
    <height>20</height>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>CtrlMode-RB</name>
    <pv_name>$(DEVICE):CtrlMode-RB</pv_name>
    <x>510</x>
    <y>65</y>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Ch1TypeR</name>
    <pv_name>$(DEVICE):Ch1TypeR</pv_name>
    <x>400</x>
    <y>95</y>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Ch2TypeR</name>
    <pv_name>$(DEVICE):Ch2TypeR</pv_name>
    <x>400</x>
    <y>125</y>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Channel 1</name>
    <text>Channel 1:</text>
    <x>270</x>
    <y>95</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Channel 2</name>
    <text>Channel 2:</text>
    <x>270</x>
    <y>125</y>
    <width>120</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Ch1</name>
    <actions>
      <action type="open_display">
        <file>lm510_lt.bob</file>
        <macros>
          <DEVICE>$(DEVICE_CH1)</DEVICE>
        </macros>
        <target>standalone</target>
        <description>Open Display</description>
      </action>
    </actions>
    <pv_name>$(DEVICE):Ch1DevNameR</pv_name>
    <text></text>
    <x>510</x>
    <y>95</y>
    <width>280</width>
    <height>20</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Ch2</name>
    <actions>
      <action type="open_display">
        <file>lm510_lt.bob</file>
        <macros>
          <DEVICE>$(DEVICE_CH2)</DEVICE>
        </macros>
        <target>standalone</target>
        <description>Open Display</description>
      </action>
    </actions>
    <pv_name>$(DEVICE):Ch2DevNameR</pv_name>
    <text></text>
    <x>510</x>
    <y>125</y>
    <width>280</width>
    <height>20</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Reset Button</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Reset</description>
      </action>
    </actions>
    <pv_name>$(DEVICE):Rst</pv_name>
    <x>510</x>
    <y>160</y>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
