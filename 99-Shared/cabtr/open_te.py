from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.pv import PVFactory

macros = widget.getEffectiveMacros()

controller = macros.getValue("DEVICE")

pv     = PVFactory.getPV(widget.getPropertyValue("pv_name"))
device = pv.read()
PVFactory.releasePV(pv)
device = device.value

if device:
    dmacros = dict()
    for k in macros.getNames():
        dmacros[k] = macros.getValue(k)

    dmacros["CONTROLLER"] = controller
    dmacros["DEVICE"]     = device

    ScriptUtil.openDisplay(widget, "cabtr_te.bob", "STANDALONE", dmacros)
