from org.csstudio.display.builder.runtime.script import PVUtil
import java.util.ArrayList as ArrayList

items = PVUtil.getStringArray(pvs[0])
arraylist = ArrayList(len(items))
for item in items:
    arraylist.add(item)
widget.setItems(arraylist)
